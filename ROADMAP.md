# L'idée

Ca pourrait être utile et résoudre des questions organiques d'avoir un robot discord qui puisse se charger de taches automatisées sur la collecte de consentement, le rappel de statuts réfractaire, la gestion du rôle réfractaire par ses membres.

## Fonctionnalités

### Gestion du renouvellement des modérateurs

Il semble que ça puisse être judicieux de nommer les modérateurs sur le mode d'une élection sans candidats.

- tous les mois / 3 mois / 6 mois (?)
- le bot demande a chaque réfractaire de nommer plusieurs modérateurs possibles (maximum 5) parmi les autres réfractaires, qu'ils soient déjà modérateurs ou pas
- chaque réfractaire donne une liste de réfractaires en qui iel a confiance pour tenir ce rôle
- au bout de 8 jours le résultat est publié sur un canal réfractaire
- la méthode pour savoir les conditions nécessaires pour être sur la liste des modérateurs ressentis est à discuter:
  - minimum de voix: toutes celleux qui recueillent au moins 5 voix ? au moins 10% des suffrages ?
  - shortlist: les 5 qui reçoivent le plus de voix sont sur la liste
- chaque réfractaire qui aura été ressenti a le choix de refuser le mandat

Commentaires bienvenus:



### Rappel du status réfractaire

Il est fréquent que des gens partent de discord au bout d'un moment. Du coup la liste des réfractaires contient des gens inactifs. En début d'année Myriam avait contacté tous les réfractaires pour savoir s'ils souhaitaient encore être actifs.

L'idée serait d'avoir le bot assistant de contacter à intervalle régulier les réfractaires pour leur demander s'il sont toujours réfractaires.

- tous les mois / 3 mois / 6 mois ?
- le bot envoie un message en privé à chaque réfractaire: êtes-vous toujours actif-ve ?
- un délai de 8 jours est laissé pour répondre:
  - oui, rien ne change
  - non, le bot enlève le rôle de réfractaire
- au bout de 8 jours, un rappel
- si au bout de 8 autres jours (donc après un total de 15 jours) il a pas de réponse, le rôle de réfractaire est enlevé et un message est envoyé pour prévenir la personne
- à discuter: si quelqu'un veut revenir dans les rangs des réfractaires, est-ce que ça doit pouvoir se faire par une commande au bot ou bien faut-il repasser par le processus d'intégration ?

Commentaires bienvenus:



### Assistance d'intégration

Possible qu'on puisse utiliser ce même assistant pour appeler à voter pour les intégrations aussi.


